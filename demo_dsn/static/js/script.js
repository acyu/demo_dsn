$(document).ready(function(){

	var browserWidth;
	var barHeight;
	var headerOffset;
	var priorOffset=0;
	var textOffSet;
	var boxOpen=false;
	headerOffset = $(".subHead").offset();

	var difference;
	var offSet=$(document).scrollTop();
	var bodyHeight = $('body').css("height");
	var gradeShade = 0.75;
	//console.log(gradeShade);

/*--------------SCROLL GRADIENT EFFECT--------------*/
	$(window).scroll(function() {
		offSet=$(document).scrollTop();
		bodyHeight = $('body').css("height");
		gradeShade = offSet/parseInt(bodyHeight.substr(0, bodyHeight.length-2)) + .75;
		var grade = offSet/parseInt(bodyHeight.substr(0, bodyHeight.length-2)) + .5;
		gradeShade = offSet/parseInt(bodyHeight.substr(0, bodyHeight.length-2)) + .75;
		$(".navbar-header").css("background-color","rgba(0,0,0," + grade + ")");
		$("#mainNav").css("background-color","rgba(0,0,0," + grade + ")");

		if(browserWidth >=900){
			$("#shade").css("background-color","rgba(0,0,0," + gradeShade + ")");
		}else{
			$("#shade").css("background-color","transparent");
		}

	/*--------------SCROLL HEADER EFFECT--------------*/
		textOffset=$("#textBlock").offset();
		//console.log(browserWidth);
		if (browserWidth > 700){
			difference=(textOffset.top - headerOffset.top)*.75;
		}
		 else {
			//console.log("else");
			difference=headerOffset.top/2;
		}
		if (browserWidth > 900){
			//difference=(textOffset.top - headerOffset.top)*.85;
		}

	/*	console.log(textOffset.top + "text");
		console.log(offSet+ "top");
		console.log(headerOffset.top + "header");
		console.log((textOffset.top - headerOffset.top) + "text-header");*/
		if (offSet <= difference){
			//console.log("enter");

			if(browserWidth <=600){
				$(".subHead").css("top","auto");
			} else {
				$(".subHead").css("top",offSet + headerOffset.top +"px");
			}
		}
		//priorOffset= offSet;
	});


/*--------------NAV BAR EFFECTS--------------*/

		$('ul.nav li.dropdown').hover(function() {
			$('ul.dropdown-menu',this).fadeIn('slow');
		}, function(){
			$('ul.dropdown-menu',this).fadeOut('fast');
		});

		$(".dropdown").hover(function(){
			if(browserWidth >=900){
			$("#shade").stop(true,true).animate({height:115,backgroundColor: 'rgba(0,0,0,0.95)'},500,"easeInOutQuint");
			}
			//$("#shade").stop(true,true).animate({},500,"easeInOutQuint");
		},function(){
			if(browserWidth >=900){
			$("#shade").stop(true,true).animate({height:barHeight,backgroundColor: "rgba(0,0,0," + gradeShade + ")"},500,"easeInOutQuint");
			}
			//$("#shade").stop(true,true).animate({},500,"easeInOutQuint");
		});


	function jsUpdateSize(){
		// Get the dimensions of the browser and display
		browserWidth = window.innerWidth ||
		document.documentElement.clientWidth ||
		document.body.clientWidth;

		browserHeight = window.innerHeight ||
		document.documentElement.clientHeight ||
		document.body.clientHeight;

		if(browserWidth >=400){
			$('#shade').css( "height","38px" );
			barHeight= $('#shade').css( "height" );
		}
		if(browserWidth >=980){
			$("#shade").css("background-color","rgba(0,0,0," + gradeShade + ")");
		}else{
			$("#shade").css("background-color","transparent");
		}
		//console.log(browserWidth + "browser");
		//console.log(browserHeight + "browser");

		if(browserHeight >=800){
			$('.carousel-inner').find('.item').css( "bottom","0px" );
			$('.carousel-inner').find('.item').css( "height",(browserHeight*0.7) + "px" );
		}

		/*if((browserHeight <=680) ||(browserWidth <=900) ){
			$('.carousel-inner').find('.item').css( "bottom","0px" );
			$('.carousel-inner').find('.item').css( "height","350px" );
		} */



	}
	//window.onload = jsUpdateSize;       // When the page first loads
	//window.onresize = jsUpdateSize;     // When the browser changes size
	$(document).ready(jsUpdateSize);       // When the page first loads
	$(window).resize(jsUpdateSize);     // When the browser changes size



/*--------------MORE OPEN TRANSITION--------------*/

	$( ".more" ).click(function( event ) {
		boxOpen=true;
		$('.pageText').fadeOut('fast',function() {
			$( ".more" ).hide();
			$(".pageText").removeClass("closed");
			$(".subHead").removeClass("hClosed");
			$(".mainImage").addClass("imageOpen");
			//$('.pageText').css( "position","absolute" );
			$('.pageText').css( "top","100%" );
			$("div.hide").removeClass("hide");
			//console.log($("#mainText").find('.two-col'));
			$("#mainText.two-col").removeClass("col-sm-12").addClass("col-sm-8");
			$("#side.two-col").removeClass("col-sm-12").addClass("col-sm-4");
			$('.pageText').show();
			//$('#textBlock').fadeIn('fast', function() {
				$('.pageText').css( "position","relative" );
				$(".pageText").stop(true,true).animate({top:0},1000,"easeInOutQuint");


			//});
		});
	});

	/*--------------SUBHEAD FADE --------------*/
	$(".galSub").stop(true,true).delay( 10000 ).animate({opacity:0},1000,"easeInOutQuint");


	var zoom = ((document.body.clientWidth)/(window.innerWidth));
	//var itemHeight=$( ".carousel-inner .active" ).find(".carousel-caption").height();
	var carHeight=$( "#galleryCarousel" ).height();
	//var indicatorLoc=$( ".indicators" ).offset();
	//$( ".carousel-caption" ).css( "top",indicatorLoc.top +itemHeight+ "px" );
	$( ".carousel-caption" ).css( "top",carHeight+ "px" );
	$( ".carousel-caption" ).hide();
	//console.log(carHeight);


	/*--------------CAPTION OPEN --------------*/
	var showOrHide= true;
	$( ".captionOpen" ).click( function() {
		var carHeight=$( "#galleryCarousel" ).height();
		//itemHeight=$("#galleryCarousel .active" ).find(".carousel-caption").outerHeight();
		$( ".carousel-caption" ).css( "top", "auto" );
		var itemHeight=$( ".carousel-inner .active" ).find(".carousel-caption").outerHeight();
		//console.log(indicatorLoc.top+"item");
		//var combo=(indicatorLoc.top-118) - itemHeight;
		$( ".carousel-caption" ).css( "top", carHeight+ "px" );
		var combo=carHeight - itemHeight;
		//console.log(combo+"combo");
		if ( showOrHide === true ) {
			$(".captionOpen").removeClass("dropup");
			$( ".carousel-caption" ).show();
			$(".carousel-caption").stop(true,true).animate({"top":combo +"px"},1000,"easeInOutQuint");
			showOrHide= false;
		} else if ( showOrHide === false ) {
			$(".captionOpen").addClass("dropup");
			$( ".carousel-caption" ).css( "top", combo+ "px" );
			//console.log(carHeight+"item");
			$(".carousel-caption").stop(true,true).animate({"top":carHeight +"px"},1000,"easeInOutQuint",function() {
				$( ".carousel-caption" ).hide();
			});

			showOrHide= true;
		}
	});
	/*-------------- CAROUSEL SCROLLER --------------*/

	var images = ($( ".indicators" ).find('li').length) * 130;
	$( ".carousel-indicators" ).css( "width",images + "px" );

	$(".scroller").mCustomScrollbar({
			horizontalScroll:true,
			advanced:{autoExpandHorizontalScroll:true,updateOnContentResize:false},
			scrollButtons:{
				enable:true
			}
	});

	/*-------------- CAROUSEL CHANGE --------------*/

	var curButton;
	var selected=0;

	$( ".carousel-indicators" ).find('li').click( function() {
		curButton = $(this).find('img').attr('alt');
		selected =$(this).index();
		changeCarousel();
	});

	$( "#galleryCarousel" ).find('.left').click( function() {
		selected -=1;
		images = $( ".indicators" ).find('li').length;
		if (selected<=0){
			selected= images-1;
		}
		curButton = $( ".indicators" ).find("[data-slide-to='" + selected + "']").find('img').attr('alt');
		//console.log(selected);
		 changeCarousel();

	});

	$( "#galleryCarousel" ).find('.right').click( function() {
		selected +=1;
		images = $( ".indicators" ).find('li').length;
		if (selected>=images){
			selected=0;
		}
			curButton = $( ".indicators" ).find("[data-slide-to='" + selected + "']").find('img').attr('alt');
		//console.log(images);
		 changeCarousel();

	});

	$('#gallery').carousel({
	  interval: false,
	  pause: true,
		wrap: true
	});
	function changeCarousel(){
		//var str;;
		$('#gallery').carousel(parseInt(selected));
		$(".picName").html(curButton);

	};

	/*-------------- GALLERY PULLDOWN --------------*/
	$(".pulldown").change(function(){ if ($(this).val()!='') { window.location.href=$(this).val(); } });


});

