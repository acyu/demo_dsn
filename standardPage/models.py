from django.db import models

# Create your models here.
from wagtail.wagtailcore.models import Page
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel

class StandardPage(Page):
  body = RichTextField(blank=True);

  content_panels = Page.content_panels + [
    FieldPanel('body', classname = 'full')
  ]

  class Meta:
    verbose_name = "Standard Page"